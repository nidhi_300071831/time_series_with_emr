
from pyspark.sql import SparkSession
from functools import reduce
import nltk
import numpy as np
from nltk.tokenize import TweetTokenizer
from collections import Counter
import re, string
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf,col,lit
from pyspark.sql.types import *  #StringType
from pyspark.sql import DataFrame
from tqdm import tqdm 
from operator import add
from collections import defaultdict
from pyspark.sql.functions import col
from functools import reduce
from pyspark.sql import DataFrame
from datetime import datetime
import pyspark.sql.functions as F
from datetime import date, timedelta
import datetime
from azure.storage.blob import BlockBlobService
import os
import sys
from time_series import time_series_main
import ast
import pandas as pd

os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable


spark = SparkSession.builder.appName("Trends").getOrCreate()
#spark.conf.set("spark.yarn.appMasterEnv.PYSPARK_PYTHON", "/usr/bin/anaconda/envs/py35new/bin/python")
#spark.conf.set("spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON", "/usr/bin/anaconda/envs/py35new/bin/python")
sas_token = "sv=2020-04-08&si=druid-read-temp&sr=d&sig=VaVglzfo6%2F1mWLzED2f6MDqNLlxNDjhVHENY75MwcL4%3D&sdd=1"
#azure_sas_url = "https://vdpstorageprod.blob.core.windows.net/pandora-druid-unload/mi_product_view_1?sv=2020-04-08&si=druid-read-temp&sr=d&sig=VaVglzfo6%2F1mWLzED2f6MDqNLlxNDjhVHENY75MwcL4%3D&sdd=1"
spark.conf.set("fs.azure.sas.pandora-druid-unload.vdpstorageprod.blob.core.windows.net", sas_token)


tknzr = TweetTokenizer(strip_handles=True, reduce_len=True)
special_chars = [ '*', '~', '!', '$',
             '%', '^',
             '&', '(', ')', '+',
             '[', ']', '{', '}',
             '.', '/', '-', '<',
             '>', '?', ':', ';',
             '`', ' ']


marketplace = ["shein.in","asos","koovs"]
end_date = date.today() - timedelta(1)
days =  timedelta(180)#180
start_date = end_date - days


sas_url = "https://vdpstorageprod.blob.core.windows.net/pandora-druid-unload/mi_product_view_1?sv=2020-04-08&si=druid-read-temp&sr=d&sig=VaVglzfo6%2F1mWLzED2f6MDqNLlxNDjhVHENY75MwcL4%3D&sdd=1"
top_level_container_name = "pandora-druid-unload"
sas_token = "sv=2020-04-08&si=druid-read-temp&sr=d&sig=VaVglzfo6%2F1mWLzED2f6MDqNLlxNDjhVHENY75MwcL4%3D&sdd=1"
blob_service = BlockBlobService(account_name="vdpstorageprod", sas_token=sas_token)
#generator = blob_service.list_blobs(self.top_level_container_name, prefix="mi_product_view_1/", delimiter="")
col_list = ["product_id","cleaned_description","cleaned_title"]





def substring_udf_(description, title, themes):
        text = description + title
        l = [1 if theme in text else 0 for theme in themes]
        return np.array(l)


def date_udf_(s_date,date):
    d_s = datetime.datetime.fromtimestamp(s_date/1000).date()
    d_t = datetime.datetime.fromtimestamp(date/1000).date()
    return d_s == d_t



def remove_emoji(text):

  emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U0001F1F2-\U0001F1F4"  # Macau flag
        u"\U0001F1E6-\U0001F1FF"  # flags
        u"\U0001F600-\U0001F64F"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U0001F1F2"
        u"\U0001F1F4"
        u"\U0001F620"
        u"\u2640-\u2642"
        "]+", flags=re.UNICODE)
  text = emoji_pattern.sub(r'', text) # no emoji
  return text



def remove_special_char_and_numbers(word):
    clean_word = ''
    for char in word:
        if char not in special_chars and not char.isdigit():
            clean_word += char
        elif char.isdigit():
            clean_word +=''
        else:
            clean_word += ' '
    #print(" ".join(clean_word.split()))
    return " ".join(clean_word.split())




def remove_hashtag_mentioned(text):
    entity_prefixes = ['@','#']
    '''for separator in string.punctuation:
        if separator not in entity_prefixes :
            text = text.replace(separator,' ')'''
    words = []
    for word in text.split():
        word = word.strip()
        if word:
            if word[0] not in entity_prefixes and word.startswith('u00')== False:
                words.append(word)
    return ' '.join(words)


def clean_text(text):
    if not text:
        return ""
    
    text = remove_emoji(text)
    list_of_words = [i.lower() for i in tknzr.tokenize(text)]
    list_of_words = [remove_special_char_and_numbers(word) for word in list_of_words]
    list_of_words = filter(lambda x: "http" not in x, list_of_words)
    text =  " ".join(list_of_words)
    text = remove_hashtag_mentioned(text)
    
    return text


def get_fashionword():
    text = spark.read.text("wasbs://myntra-datasciences@myntradatasciences.blob.core.windows.net/Nidhi/expanded_set_asos16_1.txt")
    fashionwords = [x.value for x in text.collect()][:200]
    return fashionwords 



def generate_date_list():
    #dates = [self.start_date, self.end_date]
    #date_range = [d.date() for d in pd.to_datetime(dates)]
    #date_list = list(pd.date_range(date_range[0], date_range[1]-timedelta(days=1),freq='d'))
    sdate = start_date #date(2021,6,1)
    edate = end_date #date(2021,6,15)
    date_list = [sdate+timedelta(days=x) for x in range((edate-sdate).days)]
    folder_name_list = []
    for d in date_list:
        folder_name_list.append(str(d).split(" ")[0])
    return folder_name_list



def fetch_folder_list():
    date_list = generate_date_list()
    list_of_folder_date_wise = defaultdict(list)
    new_date_list = []

    for date in date_list:
        print(date)
        #print(type(date))
        try:
            generator = blob_service.list_blobs(top_level_container_name, prefix="mi_product_view_1/"+ date, delimiter="")
            new_date_list.append(date)
            for blob in generator:
                if blob.name.endswith(".json") == False:
                    #print("\t Blob name: " + blob.name)
                    splitted_folder_name = str(blob.name).split("/")
                    if len(splitted_folder_name) == 3:
                        list_of_folder_date_wise[date].append(splitted_folder_name[2])

        except ValueError:
            print("This date data is not available")


    greatest_time_stamp_folder = {}
    for date,folder_list in list_of_folder_date_wise.items():
        #print(date, folder_list)
        int_folder_list = []
        
        for folder in folder_list:
            try:
                string_int = int(folder)
                int_folder_list.append(string_int)
            except ValueError:
                # Handle the exception
                print('corrupted folder')
        
        max_time_stamp = max(int_folder_list)
        greatest_time_stamp_folder[date] = max_time_stamp
    return new_date_list, greatest_time_stamp_folder



def write_to_azure(date_by_theme_count,fashionwords): 
    print("writing to azure")  
    OUTPUT_DIR = "wasbs://myntra-datasciences@myntradatasciences.blob.core.windows.net/Nidhi/filtered_data3/" 
    print(date_by_theme_count)
    date_by_theme_count1 =  [[x[0]]+[str(t) for t in (x[1])] for x in date_by_theme_count]
    print(date_by_theme_count1)
    print("creating data frame")
    df = spark.createDataFrame(date_by_theme_count1, ["Date"] + [x for x in fashionwords])
    print("writing")
    df.coalesce(1).write.csv(OUTPUT_DIR+"new_theme_count_6months_200.csv",header = 'true', mode="overwrite")




def processing_initial(df,fashionwords,date):
    print("pppppppppp")
    #print((df.count(), len(df.columns)))
    #date_udf = udf(date_udf_, BooleanType())
    #df = df.withColumn('is_equal_date', date_udf(*["staleness_timestamp","timestamp"]))
    
    #df = df.filter(col("is_equal_date") == True)
    #print("pppppppppp2")
    #print((df.count(), len(df.columns)))

    if df.count() == 0:
        print("returning as no value in df")
        return 

    cleaning_udf = udf(clean_text, StringType())
    #print((df.count(), len(df.columns)))
    df = df.withColumn('cleaned_description', cleaning_udf("description"))
    df = df.withColumn('cleaned_title', cleaning_udf("title"))
    #print((df.count(), len(df.columns)))
    print("calculating per_theme_count")
    per_theme_count = df.rdd.map(lambda x: ( 1, substring_udf_(x["cleaned_description"],x["cleaned_title"],fashionwords))).reduceByKey(lambda a, b: a + b).collect()
    print("calculating per_theme_count done")
    #print(per_theme_count)
    #print(date_by_theme_count)
    return per_theme_count



def main():
    fashionwords = get_fashionword()
    date_by_theme_count = []
    new_date_list, greatest_time_stamp_folder = fetch_folder_list()
    print(greatest_time_stamp_folder)
    #print(type(greatest_time_stamp_folder))
    for index,(date,folder) in tqdm(enumerate(greatest_time_stamp_folder.items())):
        
        #SeriesAppend = []
        #df = spark.read.json("wasbs://pandora-druid-unload@vdpstorageprod.blob.core.windows.net/mi_product_view_1/" + date + "/*/" )  
        df = spark.read.json("wasbs://pandora-druid-unload@vdpstorageprod.blob.core.windows.net/mi_product_view_1/" + str(date) + "/" + str(folder) )  
        print("wasbs://pandora-druid-unload@vdpstorageprod.blob.core.windows.net/mi_product_view_1/" + str(date) + "/" + str(folder))     
        df = df.select(col("timestamp"),col("staleness_timestamp"),col("product_id"),col('marketplace'),col("availability"),col("title"),col("description"),col("brand_name"), col("style_age"),col("image_url_list"),col("gender"),col("global_product_id"),col("is_live"))
        #df_filter = df.filter(col("marketplace") == "asos" )
        #df_filter = df.filter((col("marketplace").isin(marketplace)) & (col("style_age") == 0))
        df_filter = df.filter(col("style_age") == 0)
        #print(df_filter.count())
        print("initiatiating preprocessing")
        #print((df.count(), len(df.columns)))
        per_theme_count = processing_initial(df_filter,fashionwords,date)
        try:
            date_by_theme_count.append((date,per_theme_count[0][1]))
        except:
            pass
        if index % 30 == 0 and date_by_theme_count: 
            write_to_azure(date_by_theme_count,fashionwords)


def write_to_csv(theme_lists, image_list):
        empty_df = pd.DataFrame(columns = ['Remark (OK/ Reject)', 'Must have/ OK - Recommended', 'Already existing/ New','Type','Rename'])
        image_df = pd.DataFrame(image_list, columns = ['image' + str(i+1) for i in range(10)])
        themes_df = pd.DataFrame({'Themes':theme_lists})
        frames = [empty_df, themes_df, image_df]

        result = pd.concat(frames, axis = 1)
        #result.to_csv("ranked_themes.csv", index=False, header=True)
        spark_result=spark.createDataFrame(result)
        print(spark_result.show())
        spark_result.coalesce(1).write.csv(OUTPUT_DIR+"final_theme_list_with images.csv",header = 'true', mode="overwrite")
        print("write is successful")



def get_images_url(theme_list):
    theme_lists = []
    image_list = []
    
    new_date_list, greatest_time_stamp_folder = fetch_folder_list()
    print(greatest_time_stamp_folder)
    df = spark.read.json("wasbs://pandora-druid-unload@vdpstorageprod.blob.core.windows.net/mi_product_view_1/" + str(new_date_list[0]) + "/" + str(greatest_time_stamp_folder[new_date_list[0]]) ) 
    df = df.select(col("title"),col("description"),col("image_url_list"),col("primary_image_url"))
    #theme_list = columns
    for theme in theme_list:
        filtered_df = df.filter(df.description.contains(theme))
        filtered_df = filtered_df.toPandas()
        if len(filtered_df)>20:
            theme_lists.append(theme)
            img_list = []
            for index,row in filtered_df.iterrows():
                try:
                    #img_list.append(ast.literal_eval(row['image_url_list'])[0])
                    img_list.append(row['primary_image_url'])
                except:
                    #img_list.append(row['primary_image_url'])
                    pass


            image_list.append(img_list[:10])

    #print(filtered_df.head())
    write_to_csv(theme_lists, image_list)
    #return theme_lists, image_list
    #print(theme_img_dic)




main() 
OUTPUT_DIR = "wasbs://myntra-datasciences@myntradatasciences.blob.core.windows.net/Nidhi/filtered_data3/" 
data=spark.read.csv(OUTPUT_DIR+"/new_theme_count_6months_200.csv", header=True)
data = data.toPandas()
columns = list(data.columns)[1:]
#print(columns)
data[columns] = data[columns].astype(float)
#print(data.head())
theme_list = time_series_main(data)
#theme_list = columns
get_images_url(theme_list)
