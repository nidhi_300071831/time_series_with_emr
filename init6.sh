sudo /usr/bin/anaconda/bin/conda create --prefix /usr/bin/anaconda/envs/py35new python=3.6 anaconda=4.3 --yes
sudo /usr/bin/anaconda/envs/py35new/bin/pip install tqdm
sudo /usr/bin/anaconda/envs/py35new/bin/pip install azure-storage==0.36.0
sudo /usr/bin/anaconda/envs/py35new/bin/pip install nltk
sudo /usr/bin/anaconda/envs/py35new/bin/pip install numpy==1.17.4
sudo /usr/bin/anaconda/envs/py35new/bin/pip install azure==0.11.1
sudo /usr/bin/anaconda/envs/py35new/bin/pip install azure-storage-blob==0.37.1
sudo /usr/bin/anaconda/envs/py35new/bin/pip install pandas
sudo /usr/bin/anaconda/envs/py35new/bin/pip install statsmodels==0.12.2
sudo /usr/bin/anaconda/envs/py35new/bin/pip install joblib
sudo /usr/bin/anaconda/envs/py35new/bin/pip install -U scikit-learn